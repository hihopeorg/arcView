package com.amir.arcview;

public class ScrollEvent {
    private int radius = 0;
    private boolean unregistred = false;
    private int level = 0;

    public void setUnregister(boolean unregister) {
        unregistred = unregister;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}