package com.amir.arcview;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ScrollView;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;

public class ArcScrollView extends ScrollView implements Component.EstimateSizeListener,
        Component.LayoutRefreshedListener, Component.TouchEventListener, Component.ScrolledListener {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00101, "ArcScrollView");
    private static final int NOTIFY_INTERVAL_MS = 60;
    private static float screenWidth = 0f;

    private float circleRadius = 0f;
    private int strokeWidth = 0;
    private int arcWidth = 0;
    private int startOffset = 0;
    private boolean isCenterHorizontal = true;
    private float arcElevation = 0f;
    private int arcHeight = 0;
    private boolean useBestWidth = true;
    private int prevChildBottom = 0;
    private boolean animationInProgress = false;
    private int oldWidth = 0;
    private int oldHeight = 0;
    private boolean shouldOverrideEstimate = true;
    private EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
    private Runnable notifyRunnable = new Runnable() {
        @Override
        public void run() {
            notifyScroll();
        }
    };

    public ArcScrollView(Context context) {
        super(context);
        startUp(context, null);
    }

    public ArcScrollView(Context context, AttrSet attrs) {
        super(context, attrs);
        startUp(context, attrs);
    }

    public ArcScrollView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        startUp(context, attrs);
    }

    private void startUp(Context context, AttrSet attrs) {
        if (attrs != null) {
            for (int i = 0; i < attrs.getLength(); i++) {
                Attr attr = attrs.getAttr(i).get();
                switch (attr.getName()) {
                    case "radius":
                        circleRadius = attr.getDimensionValue();
                        break;
                    case "stroke_width":
                        strokeWidth = attr.getDimensionValue();
                        break;
                    case "findBestWidth":
                        useBestWidth = attr.getBoolValue();
                        break;
                    case "horizontal_center":
                        isCenterHorizontal = attr.getBoolValue();
                        break;
                    default:
                        break;
                }
            }
        }
        if (circleRadius == 0f || strokeWidth == 0) {
            throw new RuntimeException("You need to specify radius and stoke width of your ArcScrollView and they "
                    + "must not be zero");
        }

        screenWidth = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().width;
        setEstimateSizeListener(this);
        setLayoutRefreshedListener(this);
        setTouchEventListener(this);
        setScrolledListener(this);
    }

    public boolean isCenterHorizontal() {
        return isCenterHorizontal;
    }

    public int getStrokeWidth() {
        return strokeWidth;
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        if (!shouldOverrideEstimate) {
            shouldOverrideEstimate = true;
            return false;
        }
        int visibleWidth = 0;
        if (getChildCount() == 1) {
            visibleWidth = ((ArcLinearLayout) getComponentAt(0)).getWidthOfTheVisibleCircle(circleRadius,
                    strokeWidth + prevChildBottom);
        }

        shouldOverrideEstimate = false;
        if (EstimateSpec.getSize(widthMeasureSpec) < visibleWidth || !useBestWidth || visibleWidth == 0) {
            estimateSize(widthMeasureSpec, measureHeight(heightMeasureSpec));
        } else {
            estimateSize(EstimateSpec.getSizeWithMode(visibleWidth, EstimateSpec.PRECISE),
                    measureHeight(heightMeasureSpec));

        }

        return true;
    }

    private int measureHeight(int measureSpec) {
        HiLog.fatal(TAG, "measureHeight: $measureSpec");
        int result = prevChildBottom + strokeWidth;
        int specMode = EstimateSpec.getMode(measureSpec);
        int specSize = EstimateSpec.getSize(measureSpec);
        if (specMode == EstimateSpec.PRECISE) {
            result = specSize;
        }
        return EstimateSpec.getSizeWithMode(result, EstimateSpec.PRECISE);
    }

    @Override
    public void onRefreshed(Component component) {
        if (oldWidth == component.getWidth() && oldHeight == component.getHeight()) {
            return;
        }
        oldWidth = component.getWidth();
        oldHeight = component.getHeight();

        if (arcWidth == 0) {
            arcWidth = getWidth();
            arcElevation = 0f;
            startOffset = getLeft();
            setBackground(new SemiCircleDrawable(getBackgroundColor(this), SemiCircleDrawable.Direction.BOTTOM,
                    circleRadius, arcWidth, arcElevation));
        }
        if (arcHeight == 0) {
            arcHeight = getHeight();
        }
        if (getChildCount() == 1 && arcHeight != 0 && arcWidth != 0) {
            setMeasurements();
            ((ArcLinearLayout) getComponentAt(0)).headsUp();
            notifyScroll();
        }
    }

    private int getBackgroundColor(Component view) {
        int color = Color.BLACK.getValue();
        if (view.getBackgroundElement() instanceof ShapeElement) {
            color = ((ShapeElement) (view.getBackgroundElement())).getRgbColors()[0].asArgbInt();
        }
        return color;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            if (Math.pow(touchEvent.getPointerPosition(touchEvent.getIndex()).getX() - arcWidth / 2.0, 2)
                    + Math.pow(touchEvent.getPointerPosition(touchEvent.getIndex()).getY() - circleRadius, 2)
                    >= circleRadius * circleRadius || !isEnabled()) {
                return false;
            }
        }
        return false;
    }

    @Override
    public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
        notifyScroll();
        // workaround for wrong y translation after scroll, ohos setTranslationX will not change screen location
        // immediately,
        // when calculate y, it will use an old screen location milliseconds ago, so we refresh location after scroll
        // finish.
        eventHandler.removeTask(notifyRunnable);
        eventHandler.postTask(notifyRunnable, NOTIFY_INTERVAL_MS);
    }

    @Override
    public void scrolledStageUpdate(Component component, int newStage) {
        // never called in ohos so far.
        notifyScroll();
    }

    private void notifyScroll() {
        if (getComponentAt(0) instanceof ArcLinearLayout) {
            ((ArcLinearLayout) getComponentAt(0)).notifyKids(startOffset, arcElevation);
        } else {
            throw new RuntimeException("ArcScrollView can only contain ArcLinearLayout");
        }
    }

    public void setPrevChildBottom(int prevChildBottom) {
        this.prevChildBottom = prevChildBottom;
    }

    public void swapView(ArcLinearLayout view) {
        if (!animationInProgress) {
            HiLog.error(TAG, "swapView: ");
        }
        if (view == null) { //in case you want to hide the view
            AnimatorProperty animatorProperty =
                    createAnimatorProperty().alpha(0f).moveToY(strokeWidth + prevChildBottom).setDuration(500)
                            .setStateChangedListener(new Animator.StateChangedListener() {
                                @Override
                                public void onStart(Animator animator) {
                                    animationInProgress = true;
                                }

                                @Override
                                public void onStop(Animator animator) {

                                }

                                @Override
                                public void onCancel(Animator animator) {

                                }

                                @Override
                                public void onEnd(Animator animator) {
                                    setVisibility(Component.HIDE);
                                    animationInProgress = false;
                                }

                                @Override
                                public void onPause(Animator animator) {

                                }

                                @Override
                                public void onResume(Animator animator) {

                                }
                            });

            animatorProperty.setCurve(new BounceInterpolator());
            animatorProperty.start();
        } else if (getVisibility() != Component.HIDE) {
            AnimatorProperty animatorProperty =
                    createAnimatorProperty().alpha(0f).moveToY(strokeWidth + prevChildBottom).setDuration(700)
                            .setStateChangedListener(new Animator.StateChangedListener() {
                                @Override
                                public void onStart(Animator animator) {
                                    animationInProgress = true;
                                }

                                @Override
                                public void onStop(Animator animator) {

                                }

                                @Override
                                public void onCancel(Animator animator) {

                                }

                                @Override
                                public void onEnd(Animator animator) {
                                    bringBackUp(view);
                                    animationInProgress = false;
                                }

                                @Override
                                public void onPause(Animator animator) {

                                }

                                @Override
                                public void onResume(Animator animator) {

                                }
                            });
            animatorProperty.setCurve(new BounceInterpolator());
            animatorProperty.start();
        } else { //if it is already hidden make it appear
            setTranslationY(strokeWidth + prevChildBottom);
            setVisibility(Component.VISIBLE);
            bringBackUp(view);
        }
    }

    private void bringBackUp(ArcLinearLayout view) {
        if (getComponentAt(0) != null) {
            removeComponent(getComponentAt(0));
        }
        addComponent(view, 0);
        setMeasurements();
        AnimatorProperty animatorProperty =
                createAnimatorProperty().alpha(1f).moveToY(0f).setDuration(300)
                        .setStateChangedListener(new Animator.StateChangedListener() {
                            @Override
                            public void onStart(Animator animator) {
                                animationInProgress = true;
                            }

                            @Override
                            public void onStop(Animator animator) {

                            }

                            @Override
                            public void onCancel(Animator animator) {

                            }

                            @Override
                            public void onEnd(Animator animator) {
                                animationInProgress = false;
                            }

                            @Override
                            public void onPause(Animator animator) {

                            }

                            @Override
                            public void onResume(Animator animator) {

                            }
                        });
        animatorProperty.setCurve(new BounceInterpolator());
        animatorProperty.start();
        notifyScroll();
    }

    private void setMeasurements() {
        ArcLinearLayout child = (ArcLinearLayout) getComponentAt(0);
        child.setRadius(circleRadius);
        child.setContainerHeight(arcHeight);
        child.setContainerWidth(arcWidth);
    }

    private static class BounceInterpolator implements Animator.TimelineCurve {
        private static float bounce(float t) {
            return t * t * 8.0f;
        }

        public float getCurvedTime(float t) {
            t *= 1.1226f;
            if (t < 0.3535f) {
                return bounce(t);
            } else if (t < 0.7408f) {
                return bounce(t - 0.54719f) + 0.7f;
            } else if (t < 0.9644f) {
                return bounce(t - 0.8526f) + 0.9f;
            } else {
                return bounce(t - 1.0435f) + 0.95f;
            }
        }
    }
}
