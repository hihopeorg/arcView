package com.amir.arcview;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;

import java.util.List;


public class VerticalArcContainer extends ComponentContainer implements ComponentContainer.ArrangeListener,
        Component.EstimateSizeListener {

    private static final String TAG = "VerticalArcContainer";

    private int prevChildBottom = 0; //kinda doesn't need to be global
    private boolean isKnockedIn = true;
    private List<Integer> childrenStrokes;
    private float totalHeight = 0f;
    private int animationBuffer = 0;
    private ArcCallBack animationCallBack = null;

    public VerticalArcContainer(Context context) {
        super(context);
        startUp(context);
    }

    public VerticalArcContainer(Context context, AttrSet attrs) {
        super(context, attrs);
        startUp(context);
    }

    public VerticalArcContainer(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        startUp(context);
    }

    private void startUp(Context context) {
        childrenStrokes = new ArrayList<>();
        if (context instanceof ArcCallBack) {
            animationCallBack = (ArcCallBack) context;
        }
        setEstimateSizeListener(this);
        setArrangeListener(this);
    }

    public boolean isKnockedIn() {
        return isKnockedIn;
    }

    public void hideChild(int id) {
        collapseChild(getChildIndex(findComponentById(id)));
    }

    private void collapseChild(int i) {
        ArcScrollView child = (ArcScrollView) getComponentAt(i);
        int size = childrenStrokes.size();
        if (i >= 0 && i < size) {
            AnimatorProperty animatorProperty =
                    child.createAnimatorProperty().moveToY(totalHeight).setDuration(500)
                            .setStateChangedListener(new Animator.StateChangedListener() {
                        @Override
                        public void onStart(Animator animator) {

                        }

                        @Override
                        public void onStop(Animator animator) {

                        }

                        @Override
                        public void onCancel(Animator animator) {

                        }

                        @Override
                        public void onEnd(Animator animator) {
                            if (i == 0) {
                                if (animationCallBack != null) {
                                    animationCallBack.itAllKnockedOut();
                                }
                                isKnockedIn = false;
                            }
                        }

                        @Override
                        public void onPause(Animator animator) {

                        }

                        @Override
                        public void onResume(Animator animator) {

                        }
                    });
            animatorProperty.setCurve((v) -> 1.0f - (1.0f - v) * (1.0f - v));
            animatorProperty.start();
        }
    }

    public void showChild(int id) {
        expandChild(getChildIndex(findComponentById(id)));
    }

    private void expandChild(int childPosition) {
        Component child = getComponentAt(childPosition);
        if (child != null) {
            if (childPosition >= 0 && childPosition < childrenStrokes.size()) {
                AnimatorProperty animatorProperty =
                        child.createAnimatorProperty().moveToY(totalHeight - child.getHeight()).setDuration(400)
                                .setStateChangedListener(new Animator.StateChangedListener() {
                                    @Override
                                    public void onStart(Animator animator) {

                                    }

                                    @Override
                                    public void onStop(Animator animator) {

                                    }

                                    @Override
                                    public void onCancel(Animator animator) {

                                    }

                                    @Override
                                    public void onEnd(Animator animator) {
                                        if (childPosition == childrenStrokes.size() - 1) {
                                            if (animationCallBack != null) {
                                                animationCallBack.itAllKnockedIn();
                                            }
                                            isKnockedIn = true;
                                        }
                                    }

                                    @Override
                                    public void onPause(Animator animator) {

                                    }

                                    @Override
                                    public void onResume(Animator animator) {

                                    }
                                });
                animatorProperty.setCurve((v) -> (float) (Math.cos((v + 1) * Math.PI) / 2.0f) + 0.5f);
                animatorProperty.start();
            }
        }
    }

    @Override
    public boolean onArrange(int left, int top, int width, int height) {
        int rightOffset;
        int count = getChildCount();

        prevChildBottom = 0;
        for (int i = 0; i < count; i++) {
            Component child = getComponentAt(i);

            rightOffset = child.getLeft();
            if (child instanceof ArcScrollView) {
                if (((ArcScrollView) child).isCenterHorizontal()) {
                    rightOffset = getWidth() / 2 - child.getEstimatedWidth() / 2;
                }
                child.arrange(rightOffset, prevChildBottom, child.getEstimatedWidth(),
                        (int) totalHeight - prevChildBottom);

                prevChildBottom += ((ArcScrollView) child).getStrokeWidth();
            } else {
                HiLog.error(new HiLogLabel(HiLog.LOG_APP, 0x00101, getClass().getSimpleName()),
                        " onLayout else was " + "called ****MUST NOT HAPPEN***");
            }
        }
        return true;
    }

    public void knockout() {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            final int childIndex = i;
            animationBuffer++;
            getContext().getUITaskDispatcher().delayDispatch(() -> {
                animationBuffer--;
                collapseChild(childIndex);
            }, 75 * animationBuffer);
        }
    }

    public void knockIn() {
        int count = getChildCount();
        for (int i = count - 1; i >= 0; i--) {
            final int childIndex = i;
            animationBuffer++;
            getContext().getUITaskDispatcher().delayDispatch(() -> {
                animationBuffer--;
                expandChild(childIndex);
            }, 75 * animationBuffer);
        }
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int count = getChildCount();

        if (prevChildBottom == 0) {
            childrenStrokes.clear();
            for (int i = count - 1; i >= 0; i--) {
                Component child = getComponentAt(i);
                if (child instanceof ArcScrollView) {
                    ((ArcScrollView) child).setPrevChildBottom(prevChildBottom);
                    childrenStrokes.add(prevChildBottom);
                    prevChildBottom += ((ArcScrollView) child).getStrokeWidth();
                    totalHeight += ((ArcScrollView) child).getStrokeWidth();
                } else {
                    throw new RuntimeException("VerticalArcContainer can only contain ArcScrollView");
                }
            }
        }

        int desiredHeight = prevChildBottom;
        int widthMode = EstimateSpec.getMode(widthMeasureSpec);
        int widthSize = EstimateSpec.getSize(widthMeasureSpec);
        int heightMode = EstimateSpec.getMode(heightMeasureSpec);
        int heightSize = EstimateSpec.getSize(heightMeasureSpec);
        int width;
        int height;
        //Measure Width
        switch (widthMode) {
            case EstimateSpec.PRECISE:
                //Must be this size
                width = widthSize;
                break;
            case EstimateSpec.NOT_EXCEED:
                //Can't be bigger than...
                width = widthSize;
                break;
            default:
                //Be whatever you want
                width = widthMeasureSpec;
                break;
        }

        //Measure Height
        switch (heightMode) {
            case EstimateSpec.PRECISE:
                //Must be this size
                height = heightSize;
                break;
            case EstimateSpec.NOT_EXCEED:
                //Can't be bigger than...
                height = Math.min(desiredHeight, heightSize);
                break;
            default:
                //Be whatever you want
                height = desiredHeight;
                break;

        }

        //MUST CALL THIS
        setEstimatedSize(width, height);
        for (int i = 0; i < count; i++) {
            Component child = getComponentAt(i);
            measureChild(child, EstimateSpec.getSizeWithMode(width, EstimateSpec.PRECISE),
                    EstimateSpec.getSizeWithMode(height, EstimateSpec.PRECISE));
        }
        return true;
    }

    protected void measureChild(Component child, int parentWidthMeasureSpec, int parentHeightMeasureSpec) {
        final LayoutConfig lp = child.getLayoutConfig();

        final int childWidthMeasureSpec = getChildMeasureSpec(parentWidthMeasureSpec,
                getPaddingLeft() + getPaddingRight(), lp.width);
        final int childHeightMeasureSpec = getChildMeasureSpec(parentHeightMeasureSpec,
                getPaddingTop() + getPaddingBottom(), lp.height);

        child.estimateSize(childWidthMeasureSpec, childHeightMeasureSpec);
    }

    public static int getChildMeasureSpec(int spec, int padding, int childDimension) {
        int specMode = EstimateSpec.getMode(spec);
        int specSize = EstimateSpec.getSize(spec);

        int size = Math.max(0, specSize - padding);

        int resultSize = 0;
        int resultMode = 0;

        switch (specMode) {
            // Parent has imposed an exact size on us
            case EstimateSpec.PRECISE:
                if (childDimension >= 0) {
                    resultSize = childDimension;
                    resultMode = EstimateSpec.PRECISE;
                } else if (childDimension == LayoutConfig.MATCH_PARENT) {
                    // Child wants to be our size. So be it.
                    resultSize = size;
                    resultMode = EstimateSpec.PRECISE;
                } else if (childDimension == LayoutConfig.MATCH_CONTENT) {
                    // Child wants to determine its own size. It can't be bigger than us.
                    resultSize = size;
                    resultMode = EstimateSpec.NOT_EXCEED;
                }
                break;

            // Parent has imposed a maximum size on us
            case EstimateSpec.NOT_EXCEED:
                if (childDimension >= 0) {
                    // Child wants a specific size... so be it
                    resultSize = childDimension;
                    resultMode = EstimateSpec.PRECISE;
                } else if (childDimension == LayoutConfig.MATCH_PARENT) {
                    // Child wants to be our size, but our size is not fixed.
                    // Constrain child to not be bigger than us.
                    resultSize = size;
                    resultMode = EstimateSpec.NOT_EXCEED;
                } else if (childDimension == LayoutConfig.MATCH_CONTENT) {
                    // Child wants to determine its own size. It can't be bigger than us.
                    resultSize = size;
                    resultMode = EstimateSpec.NOT_EXCEED;
                }
                break;

            // Parent asked to see how big we want to be
            case EstimateSpec.UNCONSTRAINT:
                if (childDimension >= 0) {
                    // Child wants a specific size... let him have it
                    resultSize = childDimension;
                    resultMode = EstimateSpec.PRECISE;
                } else if (childDimension == LayoutConfig.MATCH_PARENT) {
                    // Child wants to be our size... find out how big it should be
                    resultSize = size;
                    resultMode = EstimateSpec.UNCONSTRAINT;
                } else if (childDimension == LayoutConfig.MATCH_CONTENT) {
                    // Child wants to determine its own size.... find out how big it should be
                    resultSize = size;
                    resultMode = EstimateSpec.UNCONSTRAINT;
                }
                break;
        }
        //noinspection ResourceType
        return EstimateSpec.getSizeWithMode(resultSize, resultMode);
    }
}