package com.amir.arcview;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Path;

public class SemiCircleDrawable extends ShapeElement {
    private float elevation;
    private int color;
    private Direction angle;

    public SemiCircleDrawable(int color, Direction angle, float circleRadius, int width, float elevation) {
        super();
        this.color = color;
        this.angle = angle;
        this.elevation = elevation;
        setRgbColor(RgbColor.fromArgbInt(color));
        setShape(ShapeElement.PATH);
        Path path = new Path();
        path.addCircle(width / 2f,
                circleRadius + elevation,
                circleRadius - elevation, Path.Direction.CLOCK_WISE);
        setPath(path);
    }

    enum Direction {
        LEFT, RIGHT, TOP, BOTTOM
    }

    ;

    public int getColor() {
        return color;
    }

    /**
     * A 32bit color not a color resources.
     *
     * @param color
     */
    public void setColor(int color) {
        this.color = color;
        setRgbColor(RgbColor.fromArgbInt(color));
    }

    @Override
    public void setAlpha(int alpha) {
        // Has no effect
    }
}