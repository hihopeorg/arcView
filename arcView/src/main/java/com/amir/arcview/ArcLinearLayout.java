package com.amir.arcview;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

public class ArcLinearLayout extends DirectionalLayout implements ComponentContainer.ArrangeListener {
    private static final int MIN_PADDING = 30;
    private float radiusPow2 = 0f;
    private float arcElevation = 0f;
    private float radius = 0f;
    private int containerWidth = 0;
    private int containerHeight = 0;
    private int startOffset = 0;
    private float itemsOffset = 0f;

    public ArcLinearLayout(Context context) {
        super(context);
        starUp(context, null);
    }

    public ArcLinearLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        starUp(context, attrs);
    }

    public ArcLinearLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        starUp(context, attrs);
    }

    private void starUp(Context context, AttrSet attrs) {
        int minimumPadding = AttrHelper.vp2px(MIN_PADDING, getContext());
        setOrientation(HORIZONTAL);
        if (attrs != null) {

            for (int i = 0; i < attrs.getLength(); i++) {
                Attr attr = attrs.getAttr(i).get();
                switch (attr.getName()) {
                    case "useMinPadding":
                        if (attr.getBoolValue()) {
                            minimumPadding = 0;
                        }
                        break;
                    case "itemsOffset":
                        itemsOffset = attr.getDimensionValue();
                        break;
                    default:
                        break;
                }
            }
        }

        setPadding(minimumPadding + getPaddingLeft(), getPaddingTop(), minimumPadding + getPaddingRight(),
                getPaddingBottom());
        setArrangeListener(this);
    }

    int getWidthOfTheVisibleCircle(float radius, int strokeWidth) {
        radiusPow2 = (float) Math.pow(radius, 2);

        //The Pythagorean is used in circle to calculate the width of the visible circle
        return (int) (2 * Math.sqrt(2 * (radius * strokeWidth) - strokeWidth * strokeWidth));
    }

    void headsUp() {
        if (getWidth() != 0) {
            float yPow2 = (float) Math.pow((containerHeight - radius), 2);
            float x = (float) (-(Math.sqrt(Math.abs(radiusPow2 - yPow2)) - containerWidth / 2)); //this is the start
            // position of the circle
            if (containerWidth > getWidth()) { //if  this layout is not as big as the width of the visible circle
                // (which also mean no scrolling)
                x += containerWidth / 2 - x - getWidth() / 2;
                float oldTranslationX = getTranslationX();
                setTranslationX((getLayoutDirection() == LayoutDirection.RTL) ? -x : x);
                if (oldTranslationX != getTranslationX()) {
                    // for ohos set translation will not refresh screen location immediately, make it calls
                    // notifyKids async
                    postLayout();
                }
            } else {
                if (getComponentParent() instanceof ArcScrollView) {
                    ((ArcScrollView) getComponentParent()).scrollTo((getWidth() - containerWidth) / 2, 0);
                    // trying to scroll to the center of the scrollView which is kinda working
                } else {
                    throw new RuntimeException("ArcLinearLayout must be parented by a ArcScrollView");
                }
            }
        }
    }

    @Override
    public boolean onArrange(int left, int top, int width, int height) {
        getContext().getUITaskDispatcher().asyncDispatch(() -> {
            headsUp();
            notifyKids(startOffset, arcElevation);
        });
        return false;
    }

    void notifyKids(int startOffset, float elevation) {
        // called when scrolling to calculate new position
        this.startOffset = startOffset;
        this.arcElevation = elevation;
        int[] pos = new int[2];
        int count = getChildCount();
        Component currChild;
        float y;
        float radiusPow2 = (float) Math.pow(radius, 2);

        for (int i = 0; i < count; i++) {
            currChild = getComponentAt(i);
            currChild.setTranslationY(containerHeight - currChild.getTop()); //making the current view invisible
            pos = currChild.getLocationOnScreen();
            pos[0] -= startOffset;

            if (pos[0] + currChild.getWidth() > 0 && currChild.getWidth() != 0) { //check also if smaller than width
                // of the
                // container
                pos[0] -= containerWidth / 2;
                float xPow2 = (float) (Math.pow((pos[0] + currChild.getWidth() / 2.0), 2));
                y = (float) (Math.abs(Math.sqrt(Math.abs(radiusPow2 - xPow2)) - radius));
                currChild.setTranslationY(y * 1.2f + elevation + itemsOffset - currChild.getTop());
            }
        }
    }

    void setRadius(float radius) {
        this.radius = radius;
    }

    void setContainerHeight(int containerHeight) {
        this.containerHeight = containerHeight;
    }

    void setContainerWidth(int containerWidth) {
        this.containerWidth = containerWidth;
    }
}