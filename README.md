# arcView

**本项目是基于开源项目arcView进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/amir5121/arcView ）追踪到原项目版本**

#### 项目介绍

- 项目名称：arcView弧形布局UI库
- 所属系列：ohos的第三方组件适配移植
- 功能：提供一套自定义搜索框控件
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/amir5121/arcView
- 原项目基线版本：未发布Release版本
- 编程语言：Java
- 外部库依赖：无

#### 效果展示

<img src="gif/ArcListView1.gif"/>

<img src="gif/ArcListView2.gif"/>

<img src="gif/ArcListView3.gif"/>

#### 安装教程

##### 方案一：

1. 下载ArcView三方库har包arcView.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在module级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'arcView', ext: 'har')
	……
}
```

##### 方案二：

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/'
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'com.amir.ohos:arcview:1.0.1'
 }
```


#### 

#### 使用说明

### 布局

注意，VerticalArcContainer子布局为一个至多个ArcScrollView, ArcScrollView子布局为0-1个ArcLinearLayout

`layout.xml`

```
<?xml version="1.0" encoding="utf-8"?>
<com.amir.arcview.VerticalArcContainer
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.ohos.com/apk/res-auto"
    ohos:id="$+id:include_buttons_scroll_view"
    ohos:height="match_content"
    ohos:width="match_parent"
    ohos:align_parent_bottom="true">

    <com.amir.arcview.ArcScrollView
        ohos:id="$+id:include_arc_buttons_temp_arc"
        ohos:height="150vp"
        ohos:width="match_parent"
        ohos:background_element="#2196F3"
        ohos:visibility="hide"
        app:findBestWidth="true"
        app:radius="550vp"
        app:stroke_width="50vp"/>

    <com.amir.arcview.ArcScrollView
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:background_element="#b71c1c"
        ohos:horizontal_center="true"
        app:findBestWidth="true"
        app:radius="400vp"
        app:stroke_width="50vp"><!--ohos:elevation="5vp"-->

        <com.amir.arcview.ArcLinearLayout
            ohos:height="match_parent"
            ohos:width="match_content"
            app:itemsOffset="7vp"
            app:useMinPadding="true">
            <Image
                ohos:id="$+id:include_buttons_stroke"
                ohos:height="43vp"
                ohos:width="43vp"
                ohos:component_description="$string:app_name"
                ohos:image_src="$media:ic_stroke"
                ohos:theme="$pattern:EditActivityImageView"/>

            <Image
                ohos:id="$+id:include_buttons_shadow"
                ohos:height="43vp"
                ohos:width="43vp"
                ohos:component_description="$string:app_name"
                ohos:image_src="$media:ic_shadow"
                ohos:theme="$pattern:EditActivityImageView"/>

            <Image
                ohos:height="43vp"
                ohos:width="43vp"
                ohos:component_description="$string:app_name"
                ohos:image_src="$graphic:ic_size"
                ohos:theme="$pattern:EditActivityImageView"/>
        </com.amir.arcview.ArcLinearLayout>

    </com.amir.arcview.ArcScrollView>
</com.amir.arcview.VerticalArcContainer>
```

### 代码

`MainAbility.java`

```
public class MainAbility extends Ability implements Component.ClickedListener {
......

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
......
        strokeArc =
                (ArcLinearLayout) LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_stroke_arc_linear_layout,
                        include_arc_buttons_temp_arc, false);
        shadowArc =
                (ArcLinearLayout) LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_shadow_arc_linear_layout,
                        include_arc_buttons_temp_arc, false);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_kick_me:
                if (include_buttons_scroll_view.isKnockedIn()) {
                    include_buttons_scroll_view.knockout();
                } else {
                    include_buttons_scroll_view.knockIn();
                }
                break;
            case ResourceTable.Id_kick_swapped:
                HiLog.fatal(TAG, "onClick: swapped");
                include_arc_buttons_temp_arc.swapView(null);
                break;
            case ResourceTable.Id_include_buttons_shadow:
                include_arc_buttons_temp_arc.swapView(shadowArc);
                break;
            case ResourceTable.Id_include_buttons_stroke:
                include_arc_buttons_temp_arc.swapView(strokeArc);
                break;
            default:
                break;
        }
    }
}

```

### 自定义属性

```xml
ArcScrollView
    radius: dimension = 弧形半径
    stroke_width: dimension = 圆弧的可见区域高度
    findBestWidth: boolean = 是否由ArcScrollView确定宽度

ArcLinearLayout
    useMinPadding: boolean = 是否添加额外padding
    itemsOffset: dimension = ArcLinearLayout顶部偏移高度
```

#### 版权和许可信息

```
Apache License 2.0
```
