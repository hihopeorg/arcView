package com.amir.arcview;

import com.ohos.test.utils.AttrUtils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static com.ohos.test.utils.AttrUtils.createAttr;
import static com.ohos.test.utils.AttrUtils.createAttrSet;
import static org.junit.Assert.*;

public class VerticalArcContainerTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testIsKnockedIn() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        VerticalArcContainer verticalArcContainer = new VerticalArcContainer(context);
        assertTrue("isKnockedIn failed, default value should be true",
                verticalArcContainer.isKnockedIn());
        try {
            Field isKnockedInField = VerticalArcContainer.class.getDeclaredField("isKnockedIn");
            isKnockedInField.setAccessible(true);
            isKnockedInField.setBoolean(verticalArcContainer, false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            assertTrue("testIsKnockedIn failed, reflection error " + e, false);
        }
        assertFalse("isKnockedIn failed, new value should be false",
                verticalArcContainer.isKnockedIn());
    }

    @Test
    public void testHideChild() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        VerticalArcContainer verticalArcContainer = new VerticalArcContainer(context);
        int strokeWidth = AttrHelper.vp2px(30, context);
        int radius = AttrHelper.vp2px(40, context);
        int id = 100;
        List<Attr> attrs = new ArrayList<>();
        attrs.add(createAttr("stroke_width", "30vp", strokeWidth, AttrUtils.AttrType.DIMENSION, context));
        attrs.add(createAttr("radius", "40vp", radius, AttrUtils.AttrType.DIMENSION, context));
        ArcScrollView arcScrollView = new ArcScrollView(context, createAttrSet(attrs));
        arcScrollView.setId(id);
        verticalArcContainer.addComponent(arcScrollView);
        assertEquals("testHideChild failed can't find component which should be hide", 0, verticalArcContainer.getChildIndex(verticalArcContainer.findComponentById(id)));
        // last part cannot be test for Animator can only be tested in UI test.
    }

    @Test
    public void testShowChild() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        VerticalArcContainer verticalArcContainer = new VerticalArcContainer(context);
        int strokeWidth = AttrHelper.vp2px(30, context);
        int radius = AttrHelper.vp2px(40, context);
        int id = 100;
        List<Attr> attrs = new ArrayList<>();
        attrs.add(createAttr("stroke_width", "30vp", strokeWidth, AttrUtils.AttrType.DIMENSION, context));
        attrs.add(createAttr("radius", "40vp", radius, AttrUtils.AttrType.DIMENSION, context));
        ArcScrollView arcScrollView = new ArcScrollView(context, createAttrSet(attrs));
        arcScrollView.setId(id);
        verticalArcContainer.addComponent(arcScrollView);
        assertEquals("testShowChild failed can't find component which should be show", 0, verticalArcContainer.getChildIndex(verticalArcContainer.findComponentById(id)));
        // last part cannot be test for Animator can only be tested in UI test.
    }

    @Test
    public void testKnockout() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        VerticalArcContainer verticalArcContainer = new VerticalArcContainer(context);
        int strokeWidth = AttrHelper.vp2px(30, context);
        int radius = AttrHelper.vp2px(40, context);
        int id = 100;
        List<Attr> attrs = new ArrayList<>();
        attrs.add(createAttr("stroke_width", "30vp", strokeWidth, AttrUtils.AttrType.DIMENSION, context));
        attrs.add(createAttr("radius", "40vp", radius, AttrUtils.AttrType.DIMENSION, context));
        ArcScrollView arcScrollView = new ArcScrollView(context, createAttrSet(attrs));
        arcScrollView.setId(id);
        verticalArcContainer.addComponent(arcScrollView);
        assertEquals("testKnockout getCount wrong", 1, verticalArcContainer.getChildCount());
        assertEquals("testKnockout failed can't find component which should be testKnockout", 0, verticalArcContainer.getChildIndex(verticalArcContainer.findComponentById(id)));
        // last part cannot be test for Animator can only be tested in UI test.
    }

    @Test
    public void testKnockIn() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        VerticalArcContainer verticalArcContainer = new VerticalArcContainer(context);
        int strokeWidth = AttrHelper.vp2px(30, context);
        int radius = AttrHelper.vp2px(40, context);
        int id = 100;
        List<Attr> attrs = new ArrayList<>();
        attrs.add(createAttr("stroke_width", "30vp", strokeWidth, AttrUtils.AttrType.DIMENSION, context));
        attrs.add(createAttr("radius", "40vp", radius, AttrUtils.AttrType.DIMENSION, context));
        ArcScrollView arcScrollView = new ArcScrollView(context, createAttrSet(attrs));
        arcScrollView.setId(id);
        verticalArcContainer.addComponent(arcScrollView);
        assertEquals("testKnockIn getCount wrong", 1, verticalArcContainer.getChildCount());
        assertEquals("testKnockIn failed can't find component which should be testKnockout", 0, verticalArcContainer.getChildIndex(verticalArcContainer.findComponentById(id)));
        // last part cannot be test for Animator can only be tested in UI test.
    }
}