package com.amir.arcview;

import com.amir.arclistview.ResourceTable;
import com.ohos.test.utils.AttrUtils.AttrType;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static com.ohos.test.utils.AttrUtils.createAttr;
import static com.ohos.test.utils.AttrUtils.createAttrSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ArcScrollViewTest {
    @Before
    public void setUp() throws Exception {
        //super.setUp();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testIsCenterHorizontal() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int strokeWidth = AttrHelper.vp2px(30, context);
        int radius = AttrHelper.vp2px(40, context);
        List<Attr> attrs = new ArrayList<>();
        attrs.add(createAttr("stroke_width", "30vp", strokeWidth, AttrType.DIMENSION, context));
        attrs.add(createAttr("radius", "40vp", radius, AttrType.DIMENSION, context));
        attrs.add(createAttr("horizontal_center", "true", true, AttrType.BOOLEAN, context));
        ArcScrollView arcScrollView = new ArcScrollView(context, createAttrSet(attrs));
        assertTrue("testIsCenterHorizontal failed, should be true but false",
                arcScrollView.isCenterHorizontal());
    }

    @Test
    public void testGetStrokeWidth() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int strokeWidth = AttrHelper.vp2px(30, context);
        int radius = AttrHelper.vp2px(40, context);
        List<Attr> attrs = new ArrayList<>();
        attrs.add(createAttr("stroke_width", "30vp", strokeWidth, AttrType.DIMENSION, context));
        attrs.add(createAttr("radius", "40vp", radius, AttrType.DIMENSION, context));
        ArcScrollView arcScrollView = new ArcScrollView(context, createAttrSet(attrs));
        assertEquals("testGetStrokeWidth failed, should be " + strokeWidth + " but " + arcScrollView.getStrokeWidth()
                , strokeWidth
                , arcScrollView.getStrokeWidth());
    }

    @Test
    public void testSetPrevChildBottom() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int strokeWidth = AttrHelper.vp2px(30, context);
        int radius = AttrHelper.vp2px(40, context);
        List<Attr> attrs = new ArrayList<>();
        attrs.add(createAttr("stroke_width", "30vp", strokeWidth, AttrType.DIMENSION, context));
        attrs.add(createAttr("radius", "40vp", radius, AttrType.DIMENSION, context));
        ArcScrollView arcScrollView = new ArcScrollView(context, createAttrSet(attrs));
        int prevChildBottom = 30;
        arcScrollView.setPrevChildBottom(prevChildBottom);
        try {
            Field prevChildBottomField = ArcScrollView.class.getDeclaredField("prevChildBottom");
            prevChildBottomField.setAccessible(true);
            int value = prevChildBottomField.getInt(arcScrollView);
            assertEquals("testSetPrevChildBottom failed field prevChildBottom's value is not what we set",
                    prevChildBottom, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            assertTrue("refrection failed " + e, false);
        }
    }

    @Test
    public void testSwapView_nullParameter() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int strokeWidth = AttrHelper.vp2px(30, context);
        int radius = AttrHelper.vp2px(40, context);
        List<Attr> attrs = new ArrayList<>();
        attrs.add(createAttr("stroke_width", "30vp", strokeWidth, AttrType.DIMENSION, context));
        attrs.add(createAttr("radius", "40vp", radius, AttrType.DIMENSION, context));
        ArcScrollView arcScrollView = new ArcScrollView(context, createAttrSet(attrs));
        int childCount1 = arcScrollView.getChildCount();
        arcScrollView.swapView(null);
        int childCount2 = arcScrollView.getChildCount();
        assertEquals("testSwapView_nullParameter failed swapView parameter is null, should not change childCount",
                childCount1, childCount2);
    }

    @Test
    public void testSwapView_hide() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int strokeWidth = AttrHelper.vp2px(30, context);
        int radius = AttrHelper.vp2px(40, context);
        List<Attr> attrs = new ArrayList<>();
        attrs.add(createAttr("stroke_width", "30vp", strokeWidth, AttrType.DIMENSION, context));
        attrs.add(createAttr("radius", "40vp", radius, AttrType.DIMENSION, context));
        ArcScrollView arcScrollView = new ArcScrollView(context, createAttrSet(attrs));

        int childCount1 = arcScrollView.getChildCount();
        arcScrollView.setVisibility(Component.HIDE);
        ArcLinearLayout arcLinearLayout1 =
                (ArcLinearLayout) LayoutScatter.getInstance(context).parse(ResourceTable.Layout_shadow_arc_linear_layout, null, false);
        arcScrollView.swapView(arcLinearLayout1);
        int childCount2 = arcScrollView.getChildCount();
        assertEquals("testSwapView_hide failed childCount2 should be childCount1 + 1, now childCount1 = " + childCount1 +
                        ", childCount2 = " + childCount2,
                childCount1 + 1, childCount2);
        Component component = arcScrollView.getComponentAt(0);
        assertEquals("testSwapView_hide failed first child is not what we set",
                arcLinearLayout1, component);
    }

    @Test
    public void testSwapView_visible() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int strokeWidth = AttrHelper.vp2px(30, context);
        int radius = AttrHelper.vp2px(40, context);
        List<Attr> attrs = new ArrayList<>();
        attrs.add(createAttr("stroke_width", "30vp", strokeWidth, AttrType.DIMENSION, context));
        attrs.add(createAttr("radius", "40vp", radius, AttrType.DIMENSION, context));
        ArcScrollView arcScrollView = new ArcScrollView(context, createAttrSet(attrs));

        int childCount1 = arcScrollView.getChildCount();
        arcScrollView.setVisibility(Component.VISIBLE);
        ArcLinearLayout arcLinearLayout1 =
                (ArcLinearLayout) LayoutScatter.getInstance(context).parse(ResourceTable.Layout_shadow_arc_linear_layout, null, false);
        arcScrollView.swapView(arcLinearLayout1);
        int childCount2 = arcScrollView.getChildCount();
        assertEquals("testSwapView_visible failed in visible state should not addChild immediately now childCount1 = "
                        + childCount1 + ", childCount2 = " + childCount2,
                childCount1, childCount2);
        // last part cannot be test for Animator can only be tested in UI test.
    }

}