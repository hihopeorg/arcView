package com.amir.arclistview;

import com.amir.arcview.ArcLinearLayout;
import com.amir.arcview.ArcScrollView;
import com.amir.arcview.VerticalArcContainer;
import com.ohos.test.utils.EventHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ScrollView;
import ohos.app.Context;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ExampleOhosTest {
    @After
    public void tearDown() {
        EventHelper.clearAbilities();
    }

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.amir.arclistview", actualBundleName);
    }

    @Test
    public void testSwipe() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        assertNotNull("testSwipe failed, can not start ability", mainAbility);
        EventHelper.waitForActive(mainAbility, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        VerticalArcContainer verticalArcContainer =
                (VerticalArcContainer) mainAbility.findComponentById(ResourceTable.Id_include_buttons_scroll_view);
        ArcScrollView arcScrollView = (ArcScrollView) verticalArcContainer.getComponentAt(1);
        int scrollX1 = arcScrollView.getScrollValue(ScrollView.AXIS_X);
        EventHelper.inputSwipe(mainAbility, arcScrollView, (int) (arcScrollView.getWidth() * 0.4), 50,
                (int) (arcScrollView.getWidth() * 0.6), 50, 1000);
        int scrollX2 = arcScrollView.getScrollValue(ScrollView.AXIS_X);
        assertNotEquals("testSwipe failed, swipe event not change scrollX value", scrollX1, scrollX2);
    }

    @Test
    public void testKnockout() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        assertNotNull("testKnockout failed, can not start ability", mainAbility);
        EventHelper.waitForActive(mainAbility, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        VerticalArcContainer verticalArcContainer =
                (VerticalArcContainer) mainAbility.findComponentById(ResourceTable.Id_include_buttons_scroll_view);
        ArcScrollView arcScrollView = (ArcScrollView) verticalArcContainer.getComponentAt(1);
        Button kickMe = (Button) mainAbility.findComponentById(ResourceTable.Id_kick_me);
        int yOnScreen1 = arcScrollView.getLocationOnScreen()[1];
        EventHelper.triggerClickEvent(mainAbility, kickMe);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int yOnScreen2 = arcScrollView.getLocationOnScreen()[1];
        assertNotEquals("testKnockout failed, kick button not change y position", yOnScreen1, yOnScreen2);
    }

    @Test
    public void testKnockIn() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        assertNotNull("testKnockIn failed, can not start ability", mainAbility);
        EventHelper.waitForActive(mainAbility, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        VerticalArcContainer verticalArcContainer =
                (VerticalArcContainer) mainAbility.findComponentById(ResourceTable.Id_include_buttons_scroll_view);
        ArcScrollView arcScrollView = (ArcScrollView) verticalArcContainer.getComponentAt(1);
        Button kickMe = (Button) mainAbility.findComponentById(ResourceTable.Id_kick_me);
        EventHelper.triggerClickEvent(mainAbility, kickMe);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int yOnScreen1 = arcScrollView.getLocationOnScreen()[1];
        EventHelper.triggerClickEvent(mainAbility, kickMe);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int yOnScreen2 = arcScrollView.getLocationOnScreen()[1];
        assertNotEquals("testKnockIn failed, testKnockIn failed, kick button not change y position", yOnScreen1, yOnScreen2);
    }

    @Test
    public void testAddView() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        assertNotNull("testAddView failed, can not start ability", mainAbility);
        EventHelper.waitForActive(mainAbility, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        VerticalArcContainer verticalArcContainer =
                (VerticalArcContainer) mainAbility.findComponentById(ResourceTable.Id_include_buttons_scroll_view);
        ArcScrollView arcScrollView = (ArcScrollView) verticalArcContainer.getComponentAt(1);
        for (int i = 0; i < 5; i++) {
            EventHelper.inputSwipe(mainAbility, arcScrollView, (int) (arcScrollView.getWidth() * 0.4), 50,
                    (int) (arcScrollView.getWidth() * 0.6), 50, 1000);
        }

        ArcScrollView hiddenArcScrollView = (ArcScrollView) verticalArcContainer.getComponentAt(0);
        int visibility1 = hiddenArcScrollView.getVisibility();
        ArcLinearLayout arcLinearLayout1 = (ArcLinearLayout) hiddenArcScrollView.getComponentAt(0);
        assertEquals("testAddView, failed first ArcScrollView is not HIDE at beginning", Component.HIDE, visibility1);
        assertNull("testAddView, failed first ArcScrollView has child at beginning", arcLinearLayout1);

        Component stroke = ((ArcLinearLayout) arcScrollView.getComponentAt(0)).getComponentAt(1);
        EventHelper.triggerClickEvent(mainAbility, stroke);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int visibility2 = hiddenArcScrollView.getVisibility();
        ArcLinearLayout arcLinearLayout2 = (ArcLinearLayout) hiddenArcScrollView.getComponentAt(0);
        assertEquals("testAddView failed, first ArcScrollView is not VISIBLE after swapView", Component.VISIBLE,
                visibility2);
        assertNotNull("testAddView failed, first ArcScrollView has no child after swapView", arcLinearLayout2);
    }

    @Test
    public void testExchangeView() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        assertNotNull("testExchangeView failed, can not start ability", mainAbility);
        EventHelper.waitForActive(mainAbility, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        VerticalArcContainer verticalArcContainer =
                (VerticalArcContainer) mainAbility.findComponentById(ResourceTable.Id_include_buttons_scroll_view);
        ArcScrollView arcScrollView = (ArcScrollView) verticalArcContainer.getComponentAt(1);
        for (int i = 0; i < 5; i++) {
            EventHelper.inputSwipe(mainAbility, arcScrollView, (int) (arcScrollView.getWidth() * 0.4), 50,
                    (int) (arcScrollView.getWidth() * 0.6), 50, 1000);
        }

        ArcScrollView firstArcScrollView = (ArcScrollView) verticalArcContainer.getComponentAt(0);
        Component stroke = ((ArcLinearLayout) arcScrollView.getComponentAt(0)).getComponentAt(1);
        Component shadow = ((ArcLinearLayout) arcScrollView.getComponentAt(0)).getComponentAt(2);
        EventHelper.triggerClickEvent(mainAbility, stroke);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ArcLinearLayout arcLinearLayout1 = (ArcLinearLayout) firstArcScrollView.getComponentAt(0);
        assertNotNull("testExchangeView failed, first ArcScrollView has no child after first call swapView",
                arcLinearLayout1);

        EventHelper.triggerClickEvent(mainAbility, shadow);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ArcLinearLayout arcLinearLayout2 = (ArcLinearLayout) firstArcScrollView.getComponentAt(0);
        assertNotNull("testExchangeView failed, first ArcScrollView has no child after second call swapView",
                arcLinearLayout1);
        assertNotEquals("testExchangeView failed, child not change after swapView", arcLinearLayout1, arcLinearLayout2);
    }

    @Test
    public void testRemoveView() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        assertNotNull("testRemoveView failed, can not start ability", mainAbility);
        EventHelper.waitForActive(mainAbility, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        VerticalArcContainer verticalArcContainer =
                (VerticalArcContainer) mainAbility.findComponentById(ResourceTable.Id_include_buttons_scroll_view);
        ArcScrollView arcScrollView = (ArcScrollView) verticalArcContainer.getComponentAt(1);
        for (int i = 0; i < 5; i++) {
            EventHelper.inputSwipe(mainAbility, arcScrollView, (int) (arcScrollView.getWidth() * 0.4), 50,
                    (int) (arcScrollView.getWidth() * 0.6), 50, 1000);
        }

        ArcScrollView firstArcScrollView = (ArcScrollView) verticalArcContainer.getComponentAt(0);
        Component stroke = ((ArcLinearLayout) arcScrollView.getComponentAt(0)).getComponentAt(1);
        EventHelper.triggerClickEvent(mainAbility, stroke);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ArcLinearLayout arcLinearLayout1 = (ArcLinearLayout) firstArcScrollView.getComponentAt(0);
        assertNotNull("testRemoveView failed, first ArcScrollView has no child after first call swapView",
                arcLinearLayout1);

        Button kickSwappedView = (Button) mainAbility.findComponentById(ResourceTable.Id_kick_swapped);
        EventHelper.triggerClickEvent(mainAbility, kickSwappedView);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int visibility = firstArcScrollView.getVisibility();
        assertEquals("testRemoveView failed, ArcScrollView should be HIDE after swapView(null)", Component.HIDE,
                visibility);
    }
}