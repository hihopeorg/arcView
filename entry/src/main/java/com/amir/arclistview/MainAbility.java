package com.amir.arclistview;

import com.amir.arcview.ArcLinearLayout;
import com.amir.arcview.ArcScrollView;
import com.amir.arcview.VerticalArcContainer;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbility extends Ability implements Component.ClickedListener {
    private HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00101, "MainActivity");
    private ArcLinearLayout strokeArc;
    private ArcLinearLayout shadowArc;
    private Button kick_me;
    private Button kick_swapped;
    private Image include_buttons_stroke;
    private Image include_buttons_shadow;
    private ArcScrollView include_arc_buttons_temp_arc;
    private VerticalArcContainer include_buttons_scroll_view;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        kick_me = (Button) findComponentById(ResourceTable.Id_kick_me);
        kick_swapped = (Button) findComponentById(ResourceTable.Id_kick_swapped);
        include_buttons_stroke = (Image) findComponentById(ResourceTable.Id_include_buttons_stroke);
        include_buttons_shadow = (Image) findComponentById(ResourceTable.Id_include_buttons_shadow);
        include_arc_buttons_temp_arc = (ArcScrollView) findComponentById(ResourceTable.Id_include_arc_buttons_temp_arc);
        include_buttons_scroll_view =
                (VerticalArcContainer) findComponentById(ResourceTable.Id_include_buttons_scroll_view);

        kick_me.setClickedListener(this);
        kick_swapped.setClickedListener(this);
        include_buttons_stroke.setClickedListener(this);
        include_buttons_shadow.setClickedListener(this);
        strokeArc =
                (ArcLinearLayout) LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_stroke_arc_linear_layout,
                        include_arc_buttons_temp_arc, false);
        shadowArc =
                (ArcLinearLayout) LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_shadow_arc_linear_layout,
                        include_arc_buttons_temp_arc, false);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_kick_me:
                if (include_buttons_scroll_view.isKnockedIn()) {
                    include_buttons_scroll_view.knockout();
                } else {
                    include_buttons_scroll_view.knockIn();
                }
                break;
            case ResourceTable.Id_kick_swapped:
                HiLog.fatal(TAG, "onClick: swapped");
                include_arc_buttons_temp_arc.swapView(null);
                break;
            case ResourceTable.Id_include_buttons_shadow:
                include_arc_buttons_temp_arc.swapView(shadowArc);
                break;
            case ResourceTable.Id_include_buttons_stroke:
                include_arc_buttons_temp_arc.swapView(strokeArc);
                break;
            default:
                break;
        }
    }
}
